# [hack-a-TON 2022](https://t.me/toncontests/36)

[The project website](http://b98010qe.beget.tech/)

[The presentation](https://1drv.ms/p/s!AmQVNLKleh4xgetIqAIAPciMP3bVsg?e=Pssvka)


## Gigachat Team
- [Andrew Kravtsov](https://t.me/Kravtandr) 
- [Danila Egorenko](https://t.me/danilaEgorenko) 
- [Denis Kirillov](https://t.me/denactive) 


## Installation

```bash
npm i
npm run start
```


## Poblem
Imagine you are a content creator and you want to get some monetization of your work. There are a lot of different services, but, generally, they have significant charges and lower limits of donations. In short, they are all inconvenient.


Moreover, creators often use popular social networks to announce events and one of them is Telegram messenger.


The official donation bot is already available in the telegram, but it also has a rather high donation threshold and the user needs to do a lot of actions to start working with it.


## Idea 
However, the solution that we see is in the TON cryptocurrency and Telegram. As for our experience, most content creators have accounts in Telegram, where they share their work, news and announcements. And that's why donations in Telegram should work amazingly.


The official donation bot is already available in Telegram, but it also has a rather high donation threshold and the user needs to do a lot of actions to start working with it.


Our idea is to implement **microdonations** to content authors through paid reactions to messages. So, for example, an artist who uploads his works in Telegram will be able to monetize his work when channel subscribers send reactions to his posts.


Telegram already has a built-in wallet that can store TON and it is available to every user, so it will be very convenient to use it as a source of donations. And in the channel settings, you need to add an option of paid reactions management.


However, within the format of the [hackathon](https://t.me/toncontests/36), it is problematic to implement our idea in its original form, due to the complexity of upgrading the telegram client.


Therefore, to implement this functionality within the framework of the [hackathon](https://t.me/toncontests/36), we have decided to create a bot that will make payments and track user reactions. To get started, the author of the channel needs to specify the address of his TON wallet and set up paid reactions and pricing in the bot. Next, subscribers who want to support the author of the channel must also link their wallets to the channel with the bot.


Let's say the author of the channel has set up a "fire" reaction to send him 0.1 TON. Now channel readers who have linked their wallets in the bot will be able to send one-click **microdonations**.


## Demonstration
[The project website](http://b98010qe.beget.tech/)

[A short youtube playlist](https://www.youtube.com/watch?v=qXbbFyNWp9A&list=PLJz306gPk_AGgpq0ZzgJPaa3aUbUcAkcM)


## License
MIT
