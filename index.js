const {adminBotServe} = require('./src/adminBotServe.js');
const {workerBotServe} = require('./src/workerBotServe.js');

adminBotServe();
workerBotServe();
