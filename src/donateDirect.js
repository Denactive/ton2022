const TonWeb = require('tonweb');
const {apiKey, privateKey} = require('./config.js');


// We didn't manage to perform TON transaction properly with the SDK
// It's impossible to get a public-private keys pair of a sender wallet
// That's why mock is used


// New secret key can be generated by `tonweb.utils.newSeed()`


async function donate(userWallet, channelWallet) {
  const providerUrl = 'https://testnet.toncenter.com/api/v2/jsonRPC'; // TON HTTP API url. Use this url for testnet
  const tonweb = new TonWeb(new TonWeb.HttpProvider(providerUrl, {apiKey})); // Initialize TON SDK

  tonweb.utils.newSeed(); // Uint8Array

  const secretKey = TonWeb.utils.base64ToBytes(privateKey); // A's private (secret) key
  const keyPair = tonweb.utils.keyPairFromSeed(secretKey); // Obtain key pair (public key and private key)
  const walletA = tonweb.wallet.create({
    publicKey: keyPair.publicKey
  });

  // mnemonics do not work (

  // const words = ['hamster','hover','abuse','eagle','orbit', 'general', 'family', 'rival', 'give', 'reunion', 'rebuild', 'illegal', 'payment','sing','attack','stove','shoulder','digital','awkward','awkward','fat','defense','coffee','between'];
  // pas = await tonMnemonic.isPasswordNeeded(words);
  // valid = await tonMnemonic.validateMnemonic(mnemonic= words);
  // console.log('valid = ', valid, pas)
  // //const seed = await tonMnemonic.mnemonicToSeed(words);
  // const keyPair2 = await tonMnemonic.mnemonicToKeyPair(mnemonic);
  // //console.log('keyPair2 = ', keyPair2.publicKey)

  // const walletA = tonweb.wallet.create({
  //   publicKey: keyPair2.publicKey
  // });
  const walletAddressA = await walletA.getAddress();
  console.log(`from ${walletAddressA.toString(true, true, true)} to ${channelWallet}`)

  const wallet = walletA
  const seqno = await wallet.methods.seqno().call() || 0;

  await wallet.methods.transfer({
    secretKey: keyPair.secretKey,
    toAddress: channelWallet,           // destination address
    amount: TonWeb.utils.toNano('0.1'), // 0.01 TON to send
    seqno: seqno,
  }).send()
}

module.exports = donate;
