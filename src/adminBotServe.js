const TelegramBot = require('node-telegram-bot-api');

const config = require('./config.js');
const [token, workerBotName] = [config.tokenAdmin, config.workerBotName];

const adminDb = require('./adminStorage.js');
const {createRecord} = require('./workerStorage.js');

const adminBot = new TelegramBot(token, {polling: true});

function handleAdminBotAnswer(msg) {
  const chatId = msg.chat.id;
  switch (adminDb[chatId].activeCommand) {
    case '/register':
      if (!('forward_from_chat' in msg) && !('forward_from' in msg) && !('forward_sender_name' in msg)) {
        return 'Should be forwarded message.';
      }
      if ('forward_from' in msg || 'forward_sender_name' in msg) {
        msg.forward_from_chat = {type: 'chat'};
      }
      if (msg.forward_from_chat.type !== 'channel') {
        return (`
This is a ${msg.forward_from_chat.type + (msg.forward_from_chat.type === 'private' ? 'channel' : '')}. \
should be public channel!
        `);
      }
      adminDb[chatId].activeData.chatId = msg.forward_from_chat.id;
      adminDb[chatId].activeData.title = msg.forward_from_chat.title;
      adminDb[chatId].activeData.wallet = '';
      adminDb[chatId].activeCommand = 'register-wallet';
      adminBot.sendMessage(chatId, `Enter your TON wallet address`);
      return '';

    case 'register-wallet':
      if (!/\w{48}/.test(msg.text)) {
        return (`
Invalid wallet address. Make sure it contains of exact 48 latin and underscope symbols.
        `);
      }
      adminDb[chatId].activeData.wallet = msg.text;
      adminDb[chatId].activeCommand = '';
      createRecord(
        msg.from, {
          id: adminDb[chatId].activeData.chatId,
          title: adminDb[chatId].activeData.title,
        },
        adminDb[chatId].activeData.wallet,
      );

      adminBot.sendMessage(chatId, `Successfully registered chat "${adminDb[chatId].activeData.title}" \
(id: ${adminDb[chatId].activeData.chatId})! congratulations!!!
      `)
      .then(adminBot.sendSticker(chatId, 'CAACAgIAAxkBAAOVYsB-e4MSR3uM_ucfq8Mku5AslHYAAqcNAAKL_5lKAAERwOj_uM_JKQQ'))
      .then(adminBot.sendMessage(chatId, `
This channel has a feature: every post can be marked with a reaction. Each reaction submit a microdonation to author \
in amount of 0,1 TON. To enable TON sharing one must register in the ${workerBotName}.
      `))
      .then(adminBot.sendMessage(chatId, `There are a few more steps:
1. Above there is a standart message to explain your followers how to enable TON sharing. Edit it as you like, \
but leave a ${workerBotName} link and explain the necessity of registration in the bot. Don\'t forget to pin the message!
2. Add ${workerBotName} to your channel.
3. Give ${workerBotName} admin rights to edit messages.
4. Create a post and see how it works!
      `));
      return '';
  }
  return '';
}

const adminBotServe = async () => {
  adminBot.setMyCommands([
    {command: '/start', description: 'show start message',},
    {command: '/register', description: 'register channel and it\'s wallet',},
    {command: '/exit', description: 'exit current command and return to the beginning',},
  ]);

  adminBot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const text = msg.text;
    console.log(`message from ${msg.from?.first_name || ''} ${msg.from?.last_name || ''} (chatId: ${chatId}):`, msg.text || msg);

    try {
      switch (text) {
        case '/start':
          if (!adminDb[chatId]) {
            adminDb[chatId] = {
              activeCommand: '',
              activeData: {},
              chats: {
                
              }
            }
          } else {
            // не затрираем уже существующие чаты
            adminDb[chatId].activeCommand = '';
            adminDb[chatId].activeData = {};
          }
          return adminBot.sendMessage(chatId, `Hello!

Let's register a channel and it's wallet to help you gain TON cryptocurrency from \
reader's reactions!

Type or click here: \/register
         `);
      
        case '/register':
          adminDb[chatId].activeCommand = '/register';
          return adminBot.sendMessage(chatId, `redirect any message from your channel to this chat.
Channel should be public. \
It is possible to make it private later.
            `);

        case '/exit':
          return adminBot.emit('message', {
            ...msg,
            text: '/start',
          });
        
        default:
          if (adminDb[chatId]?.activeCommand) {
            console.log('found answer to', adminDb[chatId].activeCommand, 'command');

            const err = handleAdminBotAnswer(msg);
            if (err) {
              return adminBot.sendMessage(chatId, err + '\nTry again');
            }
            return;
          }
          return adminBot.sendMessage(chatId, 'Unrecognized command. Say what?');
      }
    } catch (e) {
      console.error(e);
      return adminBot.sendMessage(chatId, 'Sorry. Last command has broken me.');
    };
  });
};

module.exports = {
  adminBotServe,
};
