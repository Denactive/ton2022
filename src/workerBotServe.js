const TelegramBot = require('node-telegram-bot-api');

const {token} = require('./config.js');
const {workerBotName, adminBotName} = require('./config.js');
const {db} = require('./workerStorage.js');
const donate = require('./donateDirect.js');

const bot = new TelegramBot(token, {polling: true});


const sessionDb = {};


const genKeyboardReplyMarkup = (chatId) => JSON.stringify({
  inline_keyboard: [
    [
      {text: '👍🏻', callback_data: JSON.stringify({chatId, action: 'like'})},
      {text: '👎🏻', callback_data: JSON.stringify({chatId, action: 'dislike'})},
    ],
  ],
});



const addButtonsToPost = (msg) => {
  const chatId = msg.chat.id;
  const author = msg.author_signature || 'channel admin';
  console.log(`channel_post from ${author} (chatId: ${msg.chat.id}): ${msg.text}`);

  try {
    if ('text' in msg) {
      bot.editMessageText(
        msg.text, {
        message_id: msg.message_id,
        chat_id: chatId,
        entities: msg.entities,
        reply_markup: genKeyboardReplyMarkup(
          chatId,
          author,
        ),
      });
  } else {
    bot.editMessageCaption(
      msg.caption, {
        message_id: msg.message_id,
        chat_id: chatId,
        entities: msg.entities,
        reply_markup: genKeyboardReplyMarkup(
          chatId,
          author,
        ),
      },
    )
  }
  } catch (e) {
    console.error(e);
    return bot.sendMessage(chatId, 'Sorry. Last command has broken me.');
  };
};

const handleButtonClick = (msg) => {
  const chatId = msg.message.chat.id;
  const userId = msg.from.id;
  const userWallet = db[chatId]?.members[userId];
  const channelWallet = db[chatId].wallet;

  console.log(`button click from ${msg.from?.first_name || ''} ${msg.from?.last_name || ''} (chatId: ${chatId}):`);

  if (userWallet) {
    donate(userWallet, channelWallet)
    .then(bot.sendMessage(chatId, `
You see this message as the TON share feature works in the TEST mode now 🛠.

Transaction 0,1 TON from ${userWallet} (${msg.from.first_name} ${msg.from?.last_name || ''}) \
to ${channelWallet} (${db[chatId].owner.first_name} ${db[chatId].owner.last_name}).
    `));
  } else {
    return bot.sendMessage(chatId, `
You see this message as the TON share feature works in the TEST mode now 🛠.
${msg.from?.first_name || ''} ${msg.from?.last_name || ''}, please, register your TON wallet: ${workerBotName}
    `);
  }
};


function handleWorkerBotAnswer(msg) {
  const chatId = msg.chat.id;
  switch (sessionDb[chatId].activeCommand) {
    case '/register':
      if (!('forward_from_chat' in msg) && !('forward_from' in msg) && !('forward_sender_name' in msg)) {
        return 'Should be forwarded message.';
      }
      if ('forward_from' in msg || 'forward_sender_name' in msg) {
        msg.forward_from_chat = {type: 'chat'};
      }
      if (msg.forward_from_chat.type !== 'channel') {
        return (`
This is a ${msg.forward_from_chat.type + (msg.forward_from_chat.type === 'private' ? 'channel' : '')}. \
should be public channel!
        `);
      }
      if (!db[ msg.forward_from_chat.id]) {
        return (`
The owner of the "${msg.forward_from_chat.title}" channel hasn't added payment reactions. \
Contact the owner and inform him about this bot features!
He will need to submit the registration with the ${adminBotName}.
        `);
      }
      sessionDb[chatId].chatId = msg.forward_from_chat.id;
      sessionDb[chatId].title = msg.forward_from_chat.title;
      sessionDb[chatId].activeCommand = 'register-wallet';
      bot.sendMessage(chatId, `Enter your TON wallet address`);
      return '';

    case 'register-wallet':
      if (!/[\w-]{48}/.test(msg.text)) {
        return (`
Invalid wallet address. Make sure it contains of exact 48 latin and underscope symbols.
        `);
      }
      sessionDb[chatId].wallet = msg.text;
      sessionDb[chatId].activeCommand = '';
      db[sessionDb[chatId].chatId].members[msg.from.id] = sessionDb[chatId].wallet;

      bot.sendMessage(chatId, `You have successfully gain an access to microdonations in the "${sessionDb[chatId].title}" \
chat (id: ${sessionDb[chatId].chatId})! congratulations!!!
      `)
      .then(bot.sendSticker(chatId, 'CAACAgIAAxkBAAOVYsB-e4MSR3uM_ucfq8Mku5AslHYAAqcNAAKL_5lKAAERwOj_uM_JKQQ'))
      return '';
  }
  return '';
}



const workerBotServe = async () => {
  bot.setMyCommands([
    {command: '/start', description: 'show start message',},
    {command: '/register', description: 'register in a channel',},
    {command: '/exit', description: 'exit current command and return to the beginning',},
  ]);
  
  bot.on('channel_post', addButtonsToPost);
  bot.on('callback_query', handleButtonClick);
  bot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const text = msg.text;
    console.log(`message from ${msg.from?.first_name || ''} ${msg.from?.last_name || ''} (chatId: ${chatId}):`, msg.text || msg);

    try {
      switch (text) {
        case '/start':
          sessionDb[chatId] = {
            activeCommand: '',
            chatId: '',
            wallet: '',
            title: '',
          };
          return bot.sendMessage(chatId, `Hello!

Let's register TON sharing in a channel!

Type or click here: \/register
         `);
      
        case '/register':
          sessionDb[chatId].activeCommand = '/register';
          return bot.sendMessage(chatId, `redirect any message from the channel you want to subscribe to this chat.`);

        case '/exit':
          return bot.emit('message', {
            ...msg,
            text: '/start',
          });
        
        default:
          if (sessionDb[chatId]?.activeCommand) {
            console.log('found answer to', sessionDb[chatId].activeCommand, 'command');

            const err = handleWorkerBotAnswer(msg);
            if (err) {
              return bot.sendMessage(chatId, err + '\nTry again');
            }
            return;
          }
          return bot.sendMessage(chatId, 'Unrecognized command. Say what?');
      }
    } catch (e) {
      console.error(e);
      return bot.sendMessage(chatId, 'Sorry. Last command has broken me.');
    };
  });
};



module.exports = {
  workerBotServe,
};
