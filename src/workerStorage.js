// const db = {
//   // chatId: {
//   //   owner: '',
//   //   wallet: '',
//   //   members: {
//   //     'userId': 'wallet'
//   //   },
//   // },
// };

//  TODO: remove MOCK
const db = require('../secret/mockAdminDb.js');


function createRecord(owner, chat, wallet) {
  const record = {
    title: chat.title,
    owner,
    wallet,
    members: {
    
    },
  };
  db[chat.id] = record;

  console.log('created db record:\n\n', db);

  return record;
}

module.exports = {
  db,
  createRecord,
};
