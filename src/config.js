// change it!
const configPath = '../config.json';

const config = require(configPath);

if (config.token === 'YOUR_TOKEN') {
  console.warn(`\n===========================================
Supply a config.json with valid information
===========================================\n`);
}

console.log(`App config:
  admin Bot >
    name: ${config.adminBotName}
    tokenAdmin: ${config.tokenAdmin}
  worker Bot >
    name: ${config.workerBotName}
    token: ${config.token}
  App >
    apiId: ${config.apiId}
    apiHash: ${config.apiHash}
`);

 module.exports = config;
